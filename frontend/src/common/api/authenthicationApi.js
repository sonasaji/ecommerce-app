import { client } from '../interceptor/axiosInterceptor';

export const Registration = (data) => {
  let URL = 'users';
  return client({ url: URL, data: data, method: 'post' });
};

export const SignIn = (data) => {
  let URL = 'users/sign_in';
  return client({ url: URL, data: data, method: 'post' });
};

export const Logout = () => {
  let URL = 'users/sign_out';
  return client({ url: URL, method: 'delete' });
};