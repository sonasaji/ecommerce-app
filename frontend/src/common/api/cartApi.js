import { client } from '../interceptor/axiosInterceptor';

export const AddToCart = (data) => {
  let URL = 'api/v1/cart_items';
  return client({ url: URL, data: data, method: 'post' });
};

export const GetCart = (id) => {
  let URL = `api/v1/carts/${id}`;
  return client({ url: URL, method: 'get' });
};

export const RemoveFromCart = (id) => {
  let URL = `api/v1/cart_items/${id}`;
  return client({ url: URL, method: 'delete' });
};

export const UpdateItemQuantity = (id, data) => {
  let URL = `api/v1/cart_items/${id}`;
  return client({ url: URL, data: data, method: 'patch' });
};

export const GetTotalPrice = (id) => {
  let URL = `api/v1/carts/${id}/total_price`;
  return client({ url: URL, method: 'get' });
};