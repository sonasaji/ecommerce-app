import { client } from '../interceptor/axiosInterceptor';

export const GetProductDetails = () => {
    let URL = 'api/v1/products';
    return client({ url: URL, method: 'get' });
};