import { Navigate, Route, Routes } from 'react-router-dom';
import { useEffect } from "react";
import { useSelector } from 'react-redux';
import { userInfo } from './store/auth-slice';
import ProtectedRoutes from './routes/ProtectedRoutes';
import RedirectRoute from './routes/RedirectRoute';
import Footer from './components/layout/footer';
import Header from './components/layout/header';
import Home from './pages/home';
import Signup from './pages/signup';
import Login from './pages/login';
import Product from './pages/product';
import Cart from './pages/cart';


function App() {

  const authState = useSelector(userInfo);
  const isAuth = authState.isAuthenticated ? true : false;
  useEffect(() => {
    document.title = 'Shoppie';
  });

  return (
    <>
      <Header />
      <Routes>
        <Route path='/' element={<Navigate replace to='/products' />} />
        <Route path='/home' element={<Home />} />
        <Route path='/products' element={<Product />} />
        <Route element={<RedirectRoute isLogged={isAuth} />}>
          <Route path='/login' element={<Login />} />
          <Route path='/signup' element={<Signup />} />
        </Route>

        <Route element={<ProtectedRoutes isLogged={isAuth} />}>
          <Route path='/cart' element={<Cart />} />
        </Route>
        <Route path='*' element={<Navigate to='/' replace />} />
      </Routes>
      <Footer />
    </>
  );
}

export default App;
