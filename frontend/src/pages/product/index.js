import { useCallback, useEffect, useState } from "react";
import { useNavigate} from 'react-router-dom';
import { GetProductDetails } from "../../common/api/productsApi";
import { AddToCart, GetCart } from "../../common/api/cartApi";
import ContentLoader from "react-content-loader";
import background from '../../assets/images/shopping-cart.png';
import { useSelector } from 'react-redux';
import { userInfo } from '../../store/auth-slice';

const Product = () => {
  const authState = useSelector(userInfo);
  const navigate = useNavigate();
  const [isLoaded, setIsLoaded] = useState(false);
  const [productData, setProductData] = useState(null);
  const [isCartLoaded, setIsCartLoaded] = useState(false);
  const [cartData, setCartData] = useState(null);

  //List products
  const productHandler = useCallback(() => {
    GetProductDetails()
      .then(res => {
        if (res && res.data) {
          console.log('res', res);
          setIsLoaded(true);
          setProductData(res.data);
        }
      })
      .catch(err => {
        if (err && err.response) {
          setIsLoaded(true);
          console.log('err', err.response);
        }
      });
  }, []);

  //List cart products
  const cartHandler = useCallback(() => {
    if (authState && authState.authUser && authState.authUser.cart_id){
      GetCart(authState.authUser.cart_id)
      .then(res => {
        if (res && res.data) {
          console.log('res', res);
          setIsCartLoaded(true);
          setCartData(res.data);
        }
      })
      .catch(err => {
        if (err && err.response) {
          setIsCartLoaded(true);
          console.log('err', err.response);
        }
      });
    }else{
      setIsCartLoaded(true);
    }
  }, []);


  //Add item to cart
  const addItemToCart = (product_id) => {
    if (authState && authState.authUser && authState.authUser.cart_id){
      const addItem = {
        cart_id: authState.authUser.cart_id,
        product_id: product_id,
        quantity: 1
      }
      AddToCart(addItem)
      .then(res => {
        if (res && res.data) {
          console.log('res', res);
          cartHandler();
        }
      })
      .catch(err => {
        if (err && err.response) {
          console.log('err', err.response);
        }
      });
    }else{
      navigate('/login', { replace: true });
    }
  }

  //Go to cart helper button method
  const goToCart= () => {
    navigate('/cart', { replace: true });
  }

  //Check is the product is in cart or not
  const isInCart = (product_id) =>{
    if (cartData && cartData.length){
      console.log("cartData",cartData)

      const found = cartData.find(element => element.item.product_id === product_id)
        console.log("found",found)
        if (!found){
          return (
            <>
              <button className="add_cart" onClick={addItemToCart.bind(this,product_id)}>Add to cart</button>
            </>
            )
        }else{
          return (
            <>
              <button className="go_cart" onClick={goToCart}>Go to cart</button>
            </>
          )
        }
    }else{
      return (
        <>
          <button className="add_cart" onClick={addItemToCart.bind(this,product_id)}>Add to cart</button>
        </>
      )
    }
  }

  useEffect(() => {
    productHandler();
    cartHandler();
  }, [productHandler, cartHandler]);

  return (
    <section className="pt-5 pb-4">


      
      <div className="container">
        {isLoaded && isCartLoaded ?
          <>
            <h3 className="mb-4 text-center mt-5">Products</h3>
            <div className="row rest-listing-row">
              {productData.map((item, idx) => (



                <div key={item.id} className="col-md-4 col-sm-6">
                  <div className="products container">
                    <div className="card">
                      <div className="top-div">
                        <div className="border">
                          <img src={background} className="w-100" alt="Blue Jeans Jacket" />
                        </div>
                        <span>$ {item.price}</span>
                      </div>
                      <div className="bottom-div">
                        <h3>{item.name}</h3>
                        <p>{item.description}</p>
                      </div>
                      <div className="last-section">
                        <div className="buttons">
                          {
                            isInCart(item.id)
                          }
                        </div>
                      </div>
                    </div> 
                  </div>
                </div>
              ))}
            </div>
          </>
          :
          <>
            <ContentLoader viewBox="0 0 1060 370">
              <rect x="0" y="0" rx="2" ry="2" width="270" height="25" />
              <rect x="0" y="50" rx="2" ry="2" width="300" height="200" />
              <rect x="0" y="260" rx="2" ry="2" width="250" height="25" />
              <rect x="330" y="50" rx="2" ry="2" width="300" height="200" />
              <rect x="330" y="260" rx="2" ry="2" width="250" height="25" />
              <rect x="660" y="50" rx="2" ry="2" width="300" height="200" />
              <rect x="660" y="260" rx="2" ry="2" width="250" height="25" />
            </ContentLoader>
            <ContentLoader viewBox="0 0 1060 370">
              <rect x="0" y="0" rx="2" ry="2" width="270" height="25" />
              <rect x="0" y="50" rx="2" ry="2" width="300" height="200" />
              <rect x="0" y="260" rx="2" ry="2" width="250" height="25" />
              <rect x="330" y="50" rx="2" ry="2" width="300" height="200" />
              <rect x="330" y="260" rx="2" ry="2" width="250" height="25" />
              <rect x="660" y="50" rx="2" ry="2" width="300" height="200" />
              <rect x="660" y="260" rx="2" ry="2" width="250" height="25" />
            </ContentLoader>
          </>
        }
      </div>
    </section>
  );
};

export default Product;