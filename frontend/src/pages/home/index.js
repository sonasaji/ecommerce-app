import { useSelector } from 'react-redux';
import { userInfo } from '../../store/auth-slice';

const Home = () => {
  const authState = useSelector(userInfo);
  const isAuth = authState.isAuthenticated ? true : false;
  console.log(authState)

  return (
    <section className="pt-5 pb-4">
      <div className="m-5 container text-center">
        { authState && authState.authUser && authState.authUser.user_info && authState.authUser.user_info.username ?
          <h3>Welcome <b className="user-name"><i>{authState.authUser.user_info.username}</i></b></h3>
          :
          <h3>Welcome</h3>
        }
      </div>
    </section>
  );
};

export default Home;
