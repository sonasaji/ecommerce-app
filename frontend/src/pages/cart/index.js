import { useCallback, useEffect, useState } from "react";
import { useNavigate} from 'react-router-dom';
import { formatNumber } from '../../helpers/utils';
import ContentLoader from "react-content-loader";
import { useSelector } from 'react-redux';
import { userInfo } from '../../store/auth-slice';
import { 
  GetCart,
  RemoveFromCart, 
  UpdateItemQuantity, 
  GetTotalPrice } from "../../common/api/cartApi";

const Cart = () => {

  const authState = useSelector(userInfo);
  const navigate = useNavigate();
  const [isCartLoaded, setIsCartLoaded] = useState(false);
  const [cartData, setCartData] = useState(null);
  const [priceData, setPriceData] = useState(null);

  //Handle cart products
  const cartHandler = useCallback(() => {
    if (authState && authState.authUser && authState.authUser.cart_id){
      GetCart(authState.authUser.cart_id)
      .then(res => {
        if (res && res.data) {
          console.log('res', res);
          setIsCartLoaded(true);
          setCartData(res.data);
        }
      })
      .catch(err => {
        if (err && err.response) {
          setIsCartLoaded(true);
          console.log('err', err.response);
        }
      });
    }else{
      setIsCartLoaded(true);
    }
  }, []);

  //handle total price of product
  const totalPriceHandler = useCallback(() => {
    if (authState && authState.authUser && authState.authUser.cart_id){
      GetTotalPrice(authState.authUser.cart_id)
      .then(res => {
        if (res && res.data) {
          console.log('res', res);
          setIsCartLoaded(true);
          setPriceData(res.data);
        }
      })
      .catch(err => {
        if (err && err.response) {
          setIsCartLoaded(true);
          console.log('err', err.response);
        }
      });
    }else{
      setIsCartLoaded(true);
    }
  }, []);

  //remove item from cart
  const removeItemFromCart = (cart_item_id) => {
    if (authState && authState.authUser && authState.authUser.cart_id){
      
      RemoveFromCart(cart_item_id)
      .then(res => {
        console.log("result",res);
        if (res) {
          console.log('result', res);
          cartHandler();
        }
      })
      .catch(err => {
        if (err && err.response) {
          console.log('err', err.response);
        }
      });
    }else{
      navigate('/login', { replace: true });
    }
  }

  //update item quantity
  const handleUpdateItemQty = (cart_item_id, qty, price, flag) => {
    let itemData = {
      quantity:qty
    }

    let item_price = price*qty

    if (flag && flag.flag==="inc"){
      itemData = {
        quantity: +qty + 1,
        total_price: item_price
      }
    }else{
      itemData = {
        quantity: +qty - 1,
        total_price: item_price
      }
    }
    UpdateItemQuantity(cart_item_id, itemData)
    .then(res => {
      console.log("result",res);
      if (res) {
        console.log('result', res);
        cartHandler();
        totalPriceHandler();
      }
    })
    .catch(err => {
      if (err && err.response) {
        console.log('err', err.response);
      }
    });
  }
  
  useEffect(() => {
    cartHandler();
    totalPriceHandler();
  }, [cartHandler, totalPriceHandler]);

  return (
    <section className="h-100">
      <div className="container py-5">
        {isCartLoaded ?
          <>
            
            <div className="row d-flex justify-content-center my-4">
              <div className="col-md-8">
                <div className="card mb-4">
                  <div className="card-header py-3">
                    <h5 className="mb-0">Cart({cartData?.length} items)</h5>
                  </div>
                  
                    
                  {cartData && cartData.length && 
                    <div className="card-body">
                      {cartData.map((item, idx) => (
                        <div key= {item.item.id} className="row">
                          <div className="col-lg-3 col-md-12 mb-4 mb-lg-0">
                            <div className="bg-image hover-overlay hover-zoom ripple rounded" data-mdb-ripple-color="light">
                              <img src="https://mdbcdn.b-cdn.net/img/Photos/Horizontal/E-commerce/Vertical/12a.webp"
                                className="w-100" alt="Blue Jeans Jacket" />
                            </div>
                          </div>

                          <div className="col-lg-5 col-md-6 mb-4 mb-lg-0">
                            <p><strong>{item.product.name}</strong></p>
                            <p>Description: {item.product.description}</p>
                            <button type="button" className="btn btn-danger btn-sm me-1 mb-2" data-mdb-toggle="tooltip"
                              title="Remove item" onClick={removeItemFromCart.bind(this, item.item.id)}>
                              <i className="fas fa-trash"></i>
                            </button>
                          </div>

                          <div className="col-lg-4 col-md-6 mb-4 mb-lg-0">
                            <div className="item-data d-flex mb-4">
                              <button className="btn btn-outline-danger px-3 me-2"
                                onClick={() => item.item.quantity > 1 ? handleUpdateItemQty(item.item.id, item.item.quantity, item.product.price, {flag: "dec"}) : removeItemFromCart(item.item.id) }>
                                <i className="fas fa-minus"></i>
                              </button>

                              <div className="form-outline">
                                <input id="form1" min="0" name="quantity" value={item.item.quantity} type="text" className="form-control text-center" readOnly= {true}/>
                                
                              </div>

                              <button className="btn btn-outline-primary px-3 ms-2"
                                onClick={() => handleUpdateItemQty(item.item.id, item.item.quantity, item.product.price, {flag: "inc"})}>
                                <i className="fas fa-plus"></i>
                              </button>
                            </div>
                            <p className="text-start text-md-center">
                              <strong>{formatNumber(item.item.total_price)}</strong>
                            </p>
                          </div>
                          <hr className="my-4" />
                        </div>
                      ))}
                      
                    </div>
                  }
                </div>
              </div>
              <div className="col-md-4">
                <div className="card mb-4">
                  <div className="card-header py-3">
                    <h5 className="mb-0">Summary</h5>
                  </div>
                  <div className="card-body">
                    <ul className="list-group list-group-flush">
                      <li
                        className="list-group-item d-flex justify-content-between align-items-center border-0 px-0 pb-0">
                        Products(No of items)
                        <span>{cartData?.length}</span>
                      </li>
                      <li className="list-group-item d-flex justify-content-between align-items-center px-0">
                        Shipping
                        <span>$0</span>
                      </li>
                      <li
                        className="list-group-item d-flex justify-content-between align-items-center border-0 px-0 mb-3">
                        <div>
                          <strong>Total amount</strong>
                        </div>
                        <span><strong>{formatNumber(priceData)}</strong></span>
                      </li>
                    </ul>
                    <div className="d-flex justify-content-end">
                      <button type="button" className="btn btn-secondary btn-lg btn-block">
                        Checkout
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
          </>
          :
          <>
            <ContentLoader viewBox="0 0 1060 370">
              <rect x="0" y="0" rx="2" ry="2" width="270" height="25" />
              <rect x="0" y="50" rx="2" ry="2" width="300" height="200" />
              <rect x="0" y="260" rx="2" ry="2" width="250" height="25" />
              <rect x="330" y="50" rx="2" ry="2" width="300" height="200" />
              <rect x="330" y="260" rx="2" ry="2" width="250" height="25" />
              <rect x="660" y="50" rx="2" ry="2" width="300" height="200" />
              <rect x="660" y="260" rx="2" ry="2" width="250" height="25" />
            </ContentLoader>
            <ContentLoader viewBox="0 0 1060 370">
              <rect x="0" y="0" rx="2" ry="2" width="270" height="25" />
              <rect x="0" y="50" rx="2" ry="2" width="300" height="200" />
              <rect x="0" y="260" rx="2" ry="2" width="250" height="25" />
              <rect x="330" y="50" rx="2" ry="2" width="300" height="200" />
              <rect x="330" y="260" rx="2" ry="2" width="250" height="25" />
              <rect x="660" y="50" rx="2" ry="2" width="300" height="200" />
              <rect x="660" y="260" rx="2" ry="2" width="250" height="25" />
            </ContentLoader>
          </>
        }
      </div>
    </section>
  );
};

export default Cart;