import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { ErrorMessage, Field, Form, Formik } from 'formik';
import * as Yup from 'yup';
import logo from '../../assets/images/banner1.jpg';
import { Registration } from '../../common/api/authenthicationApi.js';


const Signup = () => {
  const navigate = useNavigate();
  const [submitted, setSubmitted] = useState(false);
  const [successMessage, setSuccessMessage] = useState(false);
  const [errorMessage, setErrorMessage] = useState(false);

  const initialFormValues = {
    username: "",
    email: "",
    password: "",
    password_confirmation: ""
  };

  //Form validations
  const formValidationSchema = Yup.object({
    username: Yup
      .string()
      .trim()
      .required('The username field is required'),
    email: Yup
      .string()
      .trim()
      .required('The email field is required')
      .email('The email must be a valid email address')
      .matches(/^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,})+$/, 'The email must be a valid email address'),
    password: Yup
      .string()
      .trim()
      .required('The password field is required')
      .min(6, 'Password must contain atleast six charecters'),
    password_confirmation: Yup
      .string()
      .trim()
      .required('The password confirmation field is required')
      .min(6, 'Password must contain atleast six charecters')
  });


  return (
    <section className="log-reg-sec height-fix">
      <div className="content">
        <Formik
          initialValues={initialFormValues}
          validationSchema={formValidationSchema}
          validateOnBlur={false}
          onSubmit={(values, actions) => {
            console.log(values);
            Registration(values)
            .then(res => {
              console.log('response', res);
              setSuccessMessage(true);
              actions.resetForm();
              setSubmitted(false);
              setTimeout(() => {
                setSuccessMessage(false);
                actions.setSubmitting(false);
                navigate('/login', { replace: true });
              }, 3000);
            })
            .catch(err => {
              console.log('error', err.response)
              if (err.response){
                actions.setSubmitting(false);
                setErrorMessage(true);
                setTimeout(() => {
                setErrorMessage(false);
                }, 3000);
              }
            });
          }}
        >
          {({ isSubmitting, errors }) => (
            <div className="form-content">
              <img src={logo} alt='Signup' className="form-logo" />
              <h1 className="text-center">Sign in to continue</h1>
              {errorMessage ? <div className="alert alert-danger" role="alert">
                Registration failed
              </div> : null}
              {successMessage ? <div className="alert alert-success" role="alert">
                Successfully registered
              </div> : null}
              <Form>
                <div className="form-group p-2">
                  <label htmlFor='username'>Username<sup className="text-danger">*</sup></label>
                  <Field
                    name="username"
                    type="text"
                    className={submitted && errors && errors.username ? "form-control is-invalid" : "form-control"}
                    placeholder="Username" />
                      {submitted && <span className="text-danger small">
                        <strong>
                            <ErrorMessage name="email" />
                        </strong>
                      </span>}
                </div>
                <div className="form-group p-2">
                  <label htmlFor='email'>Email<sup className="text-danger">*</sup></label>
                  <Field
                    name="email"
                    type="text"
                    className={submitted && errors && errors.email ? "form-control is-invalid" : "form-control"}
                    placeholder="Email" />
                      {submitted && <span className="text-danger small">
                        <strong>
                          <ErrorMessage name="email" />
                        </strong>
                      </span>}
                </div>
                
                <div className="form-group p-2">
                  <label htmlFor='password'>Password<sup className="text-danger">*</sup></label>
                  <Field
                    name="password"
                    type="password"
                    className={submitted && errors && errors.password ? "form-control is-invalid" : "form-control"}
                    placeholder="Password" />
                      {submitted && <span className="text-danger small">
                        <strong>
                          <ErrorMessage name="password" />
                        </strong>
                      </span>}
                </div>

                <div className="form-group p-2">
                  <label htmlFor='password_confirmation'>Confirm Password<sup className="text-danger">*</sup></label>
                  <Field
                    name="password_confirmation"
                    type="password"
                    className={submitted && errors && errors.password_confirmation ? "form-control is-invalid" : "form-control"}
                    placeholder="Confirm Password" />
                      {submitted && <span className="text-danger small">
                        <strong>
                          <ErrorMessage name="password_confirmation" />
                        </strong>
                      </span>}
                </div>

                <div className="form-group p-2">
                  <button className="btn btn-register w-100 mt-5"
                    type='submit'
                    disabled={isSubmitting}
                    onClick={() => { setSubmitted(true) }}>Sign in</button>
                </div>
              </Form>
            </div>
          )}
        </Formik>
      </div>
    </section>
  );
};

export default Signup;
