class Cart < ApplicationRecord
  belongs_to :user
  has_many :cart_items, dependent: :destroy
  has_many :products, through: :cart_items, dependent: :destroy

  # Get total price of products in cart
  def products_total_price
    sum = 0
    self.cart_items.each do |item|
      sum+= item.total_price
    end
    return sum
  end
end
