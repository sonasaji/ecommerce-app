class Api::V1::ProductsController < ApplicationController
  before_action :set_product, only: %i[ show update destroy ]

  # List all products
  def index
    @products = Product.all
    render json: @products
  end

  # create product
  def create
    @product = Product.new(product_params)
    if @product.save
      render json: @product, status: :created
    else
      render json: @product.errors, status: :unprocessable_entity
    end
  end

  #update product
  def update
    if @product.update(product_params)
      render json: @product
    else
      render json: @product.errors, status: :unprocessable_entity
    end
  end

  # delete product
  def destroy
    @product.destroy
  end

  private
    #find product
    def set_product
      @product = Product.find(params[:id])
    end

    #permit product params
    def product_params
      params.require(:product).permit(:name, :description, :price)
    end
end
