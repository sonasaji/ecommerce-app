class Api::V1::RegistrationsController < Devise::RegistrationsController
  respond_to :json

  #create user
  def create
    build_resource(sign_up_params)
    if resource.save
      @cart = Cart.create(user_id: resource.id)
      render json: { message: 'Successfully Signed up.', resource: resource }, status: :ok
    else
      render json: { message: "Signed up failure." }
    end
  end 

  private

    #permit user signin params
    def sign_up_params
      params.require(:registration).permit(:email, :username, :role, :password, :password_confirmation)
    end
end
